FROM webdevops/php-nginx:8.1

# informix setup
ENV INFORMIXDIR=/opt/ibm/informix
ENV LD_LIBRARY_PATH=/opt/ibm/informix/lib:/opt/ibm/informix/lib/esql:/opt/ibm/informix/lib/client:/opt/ibm/informix/lib/cli:
COPY ./informix /opt/ibm/informix
COPY ./docker-php-ext-pdo_informix.ini /usr/local/etc/php/conf.d/
COPY ./extensions/ /usr/local/lib/php/extensions/no-debug-non-zts-20210902/

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    zip \
    unzip \
    wget \
    git \
    zlib1g-dev \
    libpng-dev \
    unzip \
    gnupg2 \
    apt-transport-https \
    libc6 \
    libssl1.0 \
    libldap2-dev \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    libzip-dev \
    libicu-dev && \
    docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu && \
    docker-php-ext-configure gd --with-jpeg --with-freetype && \
    docker-php-ext-configure intl && \
    docker-php-ext-install -j$(nproc) \
    pdo_mysql \
    ldap \
    gd \
    zip \
    intl \
    bcmath \
    opcache && \
    pecl install xdebug && \
    docker-php-ext-enable xdebug && \
    apt-get clean && \
    rm -rf /var/cache/apt/lists && \
    rm -rf /var/tmp/* /tmp/*