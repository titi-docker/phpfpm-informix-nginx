/****************************************************************************
 *
 * Licensed Materials - Property of IBM and/or HCL
 *
 * IBM Informix Client-SDK
 * Copyright IBM Corporation 1997, 2008. All rights reserved.
 * (c) Copyright HCL Technologies Ltd. 2017.  All Rights Reserved. 
 *
 *  Title:       sqlxtype.h
 *  Description: Type codes of the X/OPEN SQL data types.
 *
 ***************************************************************************
 */

#ifndef SQLXTYPE_H_INCLUDED
#define SQLXTYPE_H_INCLUDED

#define XSQLCHAR	1
#define XSQLDECIMAL	3
#define XSQLINT		4
#define XSQLSMINT	5
#define XSQLFLOAT	6

#endif /* SQLXTYPE_H_INCLUDED */
