/****************************************************************************
 *
 * Licensed Materials - Property of IBM and/or HCL
 *
 * IBM Informix Client-SDK
 * Copyright IBM Corporation 1997, 2008. All rights reserved.
 * (c) Copyright HCL Technologies Ltd. 2017.  All Rights Reserved. 
 *
 *  Title:       mi.h
 *  Description: Includes MIAPI public header files
 *
 ***************************************************************************
 */

#ifndef _MI_H_
#define _MI_H_

#include "milib.h"
#include "miback.h"
#include "milo.h"
#include "miloback.h"
#include "mitrace.h"

#endif /* _MI_H_ */
